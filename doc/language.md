<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

CoVeriTeam is a tool to compose verification actors and execute the composition.
The tool takes as input a CoVeriTeam program written in the CoVeriTeam language.
Such a program can define the actors, artifacts, and execute the actors.
 
This document provides an introduction to the CoVeriTeam language.
We explain the language and its syntax with the help of examples.

In the simplest form a CoVeriTeam program would create an `actor` and then `execute` it on the provided artifacts.
Simplest actors are atomic actors that can be created using an actor definition file. 
We can compose the atomic actors to create more complex actors.

The following simple program (taken from [here](../examples/verifier.cvt)) describes execution of a verifier.
```
// Create verifier from external-actor definition file
ver = ActorFactory.create(ProgramVerifier, actor_def_path);

// Print type information of the created ProgramVerifier
print(ver);

// Prepare example inputs
prog = ArtifactFactory.create(Program, prog_path);
spec = ArtifactFactory.create(BehaviorSpecification, spec_path);
inputs = {'program':prog, 'spec':spec};

// Execute the verifier on the inputs
res = execute(ver, inputs);
print(res);
```
The variables `actor_def_path`, `prog_path`, and `spec_path` are provided by the user
when executing CoVeriTeam.

### Actors and Artifacts
At first, we create an actor of type `ProgramVerifier` from the actor definition
 file using the `ActorFactory` and assign it to the variable `ver`.  
Then, we use the `print` statement to print information about the actor `ver`.  
Then, we create artifacts of the type `Program` and `BehaviorSpecification` using
`ArtifactFactory` and the paths provided by the user.
Then we create a dictionary mapping artifact names to artifacts.
This dictionary is then provided to the `execute` command along with the actor to execute.
At last we `print` the result of the execution.

#### Available Actors and Artifacts
The following actors are available at present in CoVeriTeam: 
- `ProgramVerifier :: Program X Specification -> Witness X Verdict`
- `ProgramValidator :: Program X Specification X Witness X Verdict -> Witness X Verdict`
- `ProgramTester :: Program X TestSpecification -> TestSuite`
- `TestValidator :: Program X TestSuite X TestSpecification -> Verdict`
- `WitnessInstrumentor :: Program X Witness -> Program`
- `TestCriterionInstrumentor :: Program X TestSpecification -> Program`
- `TestGoalPruner :: Program X TestSpecification X TestGoal -> Program`
- `TestGoalAnnotator :: Program X TestSpecification X TestGoal -> Program`
- `CMCReducer :: Program X Condition -> Program`
- `TestGoalExtractor :: Program X TestSuite X TestSpecification -> TestGoal`
- `WitnessToTest :: Program X Specification X Witness -> TestSuite`
- `AlgorithmSelector :: Program X Specification -> AtomicActorDefinition`

The artifacts mentioned above are available in CoVeriTeam.

### ITE Composition

Lets modify our example to include validation after the verification is done.
The following snippet constructs such an actor by composing two actors.
This actor can also be executed similarly as above.
Look at the [example file](../examples/validating-verifier.cvt) for more details.
```
verifier = ActorFactory.create(ProgramVerifier, "../actors/uautomizer.yml");
validator = ActorFactory.create(ProgramValidator, "../actors/cpa-validate-violation-witnesses.yml");

// Use validator if verdict is true or false
condition = ELEMENTOF(verdict, {TRUE, FALSE});
second_component = ITE(condition, validator);

// Verifier and second component to be executed in sequence
validating_verifier = SEQUENCE(verifier, second_component);
```

Here we first create two actors of type `ProgramVerifier` and `ProgramValidator`.
Then we create an `ITE` composition for the validator.
`ITE` is an `if-then-else` composition that can be defined as:
```
ITE(condition, actor1, actor2);
```

If the `condition` is evaluated to `TRUE` then `actor1` is executed, else `actor2`.
In our example the condition checks if the artifact named `verdict` is either `TRUE` or `FALSE`,
i.e., a member of the set `{TRUE, FALSE}`.
We support the following expressions in the conditions of `ITE`:
```
exp:    NOT exp | exp AND exp | exp OR exp | exp == exp | exp != exp | INSTANCEOF(artifact_ID, artifact_type) |  ELEMENTOF(artifact_ID, STRING set)
```

Also, if the second actor is not provided and the `condition` evaluates to `false`, 
then the execution engine will try to output the available artifacts.

### Sequential Composition
Then at last we put the actor we defined with the `ITE` composition in sequence with the verifier.
The sequence will first execute the verifier and then execute the `ITE` composition.

A sequence of two actors `actor1` and `actor2` is defined as:
```
SEQUENCE(actor1, actor2);
```
Executing a sequence, first executes the `actor1`, and then `actor2`.
 It makes the artifacts produced by the execution of `actor1` available to the execution of `actor2`.

 ### Parallel Composition
 CoVeriTeam also supports `parallel` composition.
 As the name suggests, the execution of this composition executes both the components 
 and when both have finished executing only then the output of the composition is produced.

Two actors `actor1` and `actor2` can be composed in parallel by the following:
 ```
 PARALLEL(actor1, actor2);
 ```
 
### Iterative (or repeat) Composition
Another composition supported by CoVeriTeam is the iterative composition.
It can be defined as follows:
```
REPEAT(tc, a);
```

Execution of this composition would keep on executing the actor `a` iteratively
 till the termination condition `tc` is satisfied.
Upon each iteration, the artifacts generated are available as inputs for the next iteration.
`tc` is a set of variables that are tracked for fixpoint.
The iteration is terminated when the values in this set do not changed, i.e., reach a fixpoint.

An example CoVeriTeam program using the iterative composition can be found [here](../examples/repeat-condtest.cvt).

### Inbuilt Utility Actors
We also provide the following inbuilt utility actors:
#### Joiner

`Joiner` merges or joins two artifacts. It can be used as:
```
Joiner(artifact_type, artifacts set, output_artifact_name)
```

An example can be seen [here](../examples/condtest.cvt#L15).

#### Rename
The utility actor `Rename` renames artifacts. It can be used as:
```
Rename(dict_oldname_newname)
```
`Rename` takes dict of old artifact names to new artifact names and renames the artifacts.

#### Identity
The utility actor `Identity` forwards the renames artifacts. It can be used as:
```
Identity(artifact set)
```
`Identity` takes a set of artifacts and forwards them.

### TestSpecToSpec and SpecToTestSpec
The utility actors `TestSpecToSpec` and `SpecToTestSpec` convert test specification to behavior specification and vice versa.
At present, we support the formats from software verification competition and test competition.

### Functions

The language also supports encapsulation of a block of statements in functions.
The last statement in a function is a `return` statement.
A function can be defined using the following syntax:
```
fun <NAME>(<arg1>, <arg2>, ...){
    return ...;
}

```
An example of a function declaration can be seen [here](../examples/repeat-condtest.cvt#L10)