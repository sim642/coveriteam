<!--
This file is part of CoVeriTeam,
a tool for on-demand composition of cooperative verification systems:
https://gitlab.com/sosy-lab/software/coveriteam

SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# Trouble Shooting of Tool Archives with CoVeriTeam-Remote

This document provides instructions to use CoVeriTeam to execute
 the tools participating in the
 software-verification competition and software-testing competition,
 on the same infrastructure on which the competition is run.
More detailed documentation on CoVeriTeam can be found [on the main page](../README.md).
Please refer to the detailed documentation in case of need for clarification.

## Steps to follow to test a verifier or a tester
1. Clone the [CoVeriTeam repository](https://gitlab.com/sosy-lab/software/coveriteam):
   `git clone https://gitlab.com/sosy-lab/software/coveriteam.git`
2. Go to folder `examples` inside the the cloned repository.
   The `examples` folder contains two scripts: `remote_examples_verifier.sh` and `remote_examples_tester.sh`.
   These scripts contain a sample command for each of the participating tools in the competitions.
3. Please copy the command for your tool and execute it.

Here is an example command for the verifier `CPAchecker`:
```bash
../bin/coveriteam verifier.cvt \
  --input verifier_path=../actors/cpa-seq.yml \
  --input program_path=../../sv-benchmarks/c/ldv-regression/test02.c \
  --input specification_path=../../sv-benchmarks/c/properties/unreach-call.prp \
  --data-model ILP32 \
  --remote
```

This command will execute the participating tool `cpa-seq` (define in actor definition `actors/cpa-seq.yml`) on the program `c/ldv-regression/test02.c`
for the property `c/properties/unreach-call.prp` using the service (`--remote` flag).

Please also read the section on restrictions below.
More information can be found in the [documentation](index.md).

### Witness validation
We also support validating witnesses produced by a verifier by a witness validator.
The program used for this purpose is: `validating-verifier.cvt`.
Here is an example command that runs the validator `witness-linter` after the verifier `uautomizer`:
```bash
../bin/coveriteam validating-verifier.cvt \
  --input verifier_path=../actors/uautomizer.yml \
  --input validator_path=../actors/witness-linter.yml \
  --input program_path=../../sv-benchmarks/c/ldv-regression/test02.c \
  --input specification_path=../../sv-benchmarks/c/properties/unreach-call.prp \
  --data-model ILP32 \
  --remote
```

The command connects to a service that in turn tries to execute the tool on the competition infrastructure.
We have imposed some restrictions on what can be executed.
The commands complying with these restriction should be executed by the service,
otherwise the user would get an error message from the service.

### Version support
CoVeriTeam actor definitions support multiple versions of a tool archive.
For example, the actor definition for the PRTest provided multiple archive location with different versions: [PRTest actor definition](../actors/prtest.yml#L11).
A user can choose which archive to execute by providing the version when creating an actor.

This feature is available also for the CoVeriTeam service.
We have made providing version as optional. A user can specify the version for a verifier by adding `--input verifier_version=<VERSION_STRING>` on the command line.
Similarly, `--input tester_version=<VERSION_STRING>` for tester and `--input validator_version=<VERSION_STRING>` validator.
If a version is not explicitly provided then the archive specified first will be downloaded and executed.

### Trusted tool repositories
CoVeriTeam service downloads and executes tools available at the trusted location.

Trusted locations are:
1. Tool archives repositories for competitions for SVCOMP (https://gitlab.com/sosy-lab/sv-comp/archives-<YYYY>),
  and TESTCOMP (https://gitlab.com/sosy-lab/test-comp/archives-<YYYY>)
2. Tool specific trusted repository in the project group: https://gitlab.com/sosy-lab/benchmarking/coveriteam-archives.

Process for requesting a tool specific repository:
1. Please contact [Sudeep Kanav](https://www.sosy-lab.org/people/kanav/), or [Philipp Wendler](https://www.sosy-lab.org/people/wendler/) to create a repository in the project group: https://gitlab.com/sosy-lab/benchmarking/coveriteam-archives
2. Please push your tool archive to the `main` branch of the repository (create the branch if not present)
3. Update the [actor definition]((../actors)) for your tool to include a version pointing to this repository.
   (Actor definitions support multiple versions. As an example look at the [actor definition of PRTest](../actors/prtest.yml#L11).)
4. Create a merge request to merge the actor definition.

Once this merge request has been merged you should be able to use the CoVeriTeam service to execute 
the archive of your tool available on the tool specific truested repository on the competition infrastructure.

## Restrictions for execution on the competition infrastructure:
1. The actor definition (yml file) must be in the master branch of the [CoVeriTeam repository](https://gitlab.com/sosy-lab/software/coveriteam)
2. The program and property files must be in the master branch of the [sv-benchmarks repository](https://github.com/sosy-lab/sv-benchmarks)
3. Only those tool info modules can be used which are either released with the benchexec, or are in the master branch of the [benchexec repository](https://github.com/sosy-lab/benchexec).
   A user can use a url to specify a tool info module in the actor definition YAML file, instead of the name of the tool info module.
4. Only those tools are supported which are in sv competition's repository.
5. We allow only two CoVeriTeam programs to be executed: `verifier.cvt`, `tester.cvt`, and `validating-verifier.cvt` from the CoVeriTeam's repository.

We do not allow user to provide files. Only the file names are considered and the files are downloaded from the repository.
But still, the file paths should exist on your machine, these will be given to the service.
But the publicly accessible instance of the service clips these paths and downloads files from the respective repositories. 
 
## FAQs
1. Why don't I just see the output produced by my tool on `stdout`? \
   You can use the option `--verbose` for this. 
   The output you see is produced by `CoVeriTeam` for the executed program (which might execute more than one tools).
   With `-verbose` we print the output logs from the tools.
2. My tool was executed with different options than the ones defined in benchmark definitions. Why?\
   `CoVeriTeam` reads options from the [actor definition files](../actors) that
    were created from benchmark definition files but might be outdated. 
    You can either create a merge request to update the actor definition, 
    or report it in the issue tracker of verifier archives or tester archives.
3. Executing the tool using `CoVeriTeam service` behaves differently than the official pre-runs. Why?\
   We know of one scenario where this could happen.
   The service would transport the complete tool folder to the cloud,
   whereas running it with `benchexec` would transport only the required paths 
   set using the variable `REQUIRED_PATHS` in the tool info module.
   This might result in a successful execution using the service, but not with `benchexec`.
   
